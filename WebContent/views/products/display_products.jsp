<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Products</title>
    <base href="${pageContext.servletContext.contextPath}/">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="tille pt-4 pb-2">
                <h4>Một số sản phẩm gợi ý cho bạn</h4>
            </div>
            <div class="search">

            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="views/products/detail_products.jsp" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://www.sporter.vn/wp-content/uploads/2019/08/Ao-arsenal-mau-ba-1-300x300.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-1-1-300x300.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://bizweb.dktcdn.net/100/082/575/files/dia-chi-in-ao-da-bong-nhanh-chat-luong-va-uy-tin-hang-dau-tai-ha-noi.jpg?v=1522900818441"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://lh3.ggpht.com/-QkIPgxNXcd0/WfbxAG-ulPI/AAAAAAAAN3g/XsNlpJ_iL5cWI9PN0u6hMIqKTHdU-P0NQCLcBGAs/s640/Cmxq0-CXYAAwrVt.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="http://www.camisetasdefutbolbaratases.com/images/camiseta_atletico_madrid-2016_segunda_tailandia.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://lh3.ggpht.com/-wJwygT7vp5E/WfKy-KZF-4I/AAAAAAAANJg/ETsou1isSnMj5btxtEpOWX_CYXl-mwtBwCLcBGAs/s640/C_zrva6XoAAhA9g.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://www.calcioshoponlineit.com/images/maglietta_thailandia_fc_heidenheim_2018-2019_seconda.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://shop.fupa.net/Data/Images/DetailBig/fch881484-flock-nike-1-fc-heidenheim-trikot-home-kids-17-18-f657.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
        </div>

        <div class="banner mt-3">
            <img src="images/bn1.jpg" class="img-fluid">
        </div>
            <div class="tille pt-4">
                <h4>Áo câu lạc bộ</h4>
            </div>
        <div class="row mt-2">
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="http://www.camisetasdefutbolbaratases.com/images/camiseta_atletico_madrid-2016_segunda_tailandia.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://lh3.ggpht.com/-wJwygT7vp5E/WfKy-KZF-4I/AAAAAAAANJg/ETsou1isSnMj5btxtEpOWX_CYXl-mwtBwCLcBGAs/s640/C_zrva6XoAAhA9g.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://www.calcioshoponlineit.com/images/maglietta_thailandia_fc_heidenheim_2018-2019_seconda.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://shop.fupa.net/Data/Images/DetailBig/fch881484-flock-nike-1-fc-heidenheim-trikot-home-kids-17-18-f657.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
        </div>
        
                <div class="row mt-2">
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="http://www.camisetasdefutbolbaratases.com/images/camiseta_atletico_madrid-2016_segunda_tailandia.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://lh3.ggpht.com/-wJwygT7vp5E/WfKy-KZF-4I/AAAAAAAANJg/ETsou1isSnMj5btxtEpOWX_CYXl-mwtBwCLcBGAs/s640/C_zrva6XoAAhA9g.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://www.calcioshoponlineit.com/images/maglietta_thailandia_fc_heidenheim_2018-2019_seconda.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 product_card p-3 m-0">
                <a href="#" style="text-decoration: none; color: black;">
                    <div class="shadow p-3 mb-4 bg-white rounded">
                        <img src="https://shop.fupa.net/Data/Images/DetailBig/fch881484-flock-nike-1-fc-heidenheim-trikot-home-kids-17-18-f657.jpg"
                            class="img-thumbnail">
                    </div>
                    <div class="product_title" style="text-align: center;">
                        <p class="p-0 m-0">Áo câu lạc bộ Arsenal</p>
                    </div>
                    <div class="product_price" style="text-align: center;">
                        <p class="p-0 m-0">100.000 <sup>đ</sup></p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</body>

</html>