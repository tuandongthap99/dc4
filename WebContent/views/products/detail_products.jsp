<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>
<base href="${pageContext.servletContext.contextPath}/">
<link rel="stylesheet" href="css/style.css">
<link type="text/css" rel="stylesheet" href="magiczoomplus/magiczoomplus.css"/>
<script type="text/javascript" src="magiczoomplus/magiczoomplus.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<jsp:include page="../layout/header.jsp"></jsp:include>
<div class="container" style="padding-top: 100px">
	<div class="">
		<img src="https://dienmayabc.com/media/news/2501_banner_e369853d1.png"
			class="img-thumbnail">
	</div>
	<div class="row mt-3">
		<div class="col-lg-6 col-md-12">
			<a href="images/ao_02.jpg" class="MagicZoom"  data-options="selectorTrigger: hover; transitionEffect: false;"><img src="images/ao_02.jpg"></a>
		</div>
		<div class="col-lg-6 col-md-12">
			<form>
				<div>
					<h3>Áo đội tuyển Việt Nam</h3>
				</div>
				<div class="form-group mt-2">
					<label for="exampleFormControlInput1">Giá: <span>1.000.000
							VNĐ</span></label>
				</div>
				<div class="form-group">
					<label for="exampleFormControlSelect1">Kích thước</label> <select
						class="form-control" id="exampleFormControlSelect1">
						<option>S</option>
						<option>L</option>
						<option>XL</option>
						<option>M</option>
					</select>
				</div>
				<div class="form-group">
					<label for="exampleFormControlSelect1">Kích thước</label> <select
						class="form-control" id="exampleFormControlSelect1">
						<option>Xanh</option>
						<option>Đỏ</option>
						<option>Tím</option>
						<option>Vàng</option>
					</select>
				</div>
				<div class="form-group">
					<label for="exampleFormControlInput1">Chất liệu: <span>Vải</span></label>
				</div>
				<div class="form-group">
					<label for="exampleFormControlSelect1">Số lượng: </label> <input
						type="number" class="form-control input-amount" value="0"
						style="width: 11% !important; display: inline !important;">
				</div>
				<button type="button" class="btn btn-success">Đặt Hàng</button>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" class="btn btn-warning">Thêm Giỏ Hàng</button>
			</form>
		</div>
	</div>
	<div class="card mb-3" style="border: 0px !important;">
		<div class="card-body">
			<h5 class="card-title">Giới thiệu sản phẩm</h5>
			<p class="card-text">This is a wider card with supporting text
				below as a natural lead-in to additional content. This content is a
				little bit longer.This is a wider card with supporting text below as
				a natural lead-in to additional content. This content is a little
				bit longer.</p>
		</div>
		</div>
		</div>
		<jsp:include page="../layout/footer.jsp"></jsp:include>
</body>
</html>