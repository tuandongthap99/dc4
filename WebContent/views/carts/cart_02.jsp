<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${pageContext.servletContext.contextPath}/">
	<title>Giỏ hàng</title>
	<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
 <jsp:include page="../layout/header.jsp"></jsp:include>

	<div class="container" style="padding-top: 120px;">
		<div class="shopping-cart tittle mb-5">
			<h5>Giỏ hàng ( 1 sản phẩm )</h5>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="row cart-header border-bottom d-none d-md-flex d-lg-flex d-xl-flex pb-2">
					<div class="cart-item col-8">
						<h6>Sản phẩm</h6>
					</div>
					<div class="cart-item col-2">
						<h6>Đơn giá</h6>
					</div>
					<div class="cart-item col-2">
						<h6>Thành tiền</h6>
					</div>
				</div>

				<div class="row cart-body">
					<div class=" row col-lg-8">
						<a href="#" class="col-5">
							<img src="https://www.sporter.vn/wp-content/uploads/2019/08/Ao-arsenal-mau-ba-1-300x300.jpg" width="160px" height="160px" class="mt-3 img-thumbnail">
						</a>

						<div class="col-6 mt-3 cart-info p-0">
							<div class="cart-item-title"><h6>Áo câu lạc bộ arsenal</h6></div>
							<div class="cart-item-properties">Thể thao</div>
							<div class="cart-item-properties">Size: M</div>
							<div class="cart-item-properties d-block d-md-none d-lg-none d-xl-none">Đơn giá: <strong>120.000đ</strong></div>
							<div class="cart-item-properties">Số lượng:</div>
						</div>
					</div>
					<div class="col-md-2 mt-3 ml-4 d-none d-md-flex d-lg-flex d-xl-flex">120.000đ</div>
					<div class="col-md-2 mt-3 ml-1 d-none d-md-flex d-lg-flex d-xl-flex">120.000đ</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-12">
				<div class="cart-page mt-2" style="border: 1px solid #e8eaee;">
					<div class="cart-page-product-totals" style="background-color: #f8f8fa;">
						<div class="cart-page-product-subtotal px-3 py-3">
							<span class="cart-page-product-subtotal-title">Thành tiền:</span>
							<strong class="cart-page-product-subtotal-price">120.000đ</strong>
						</div>

						<div class="cart-page-product-order px-3 py-2" style="border-top: 1px solid #e8eaee;">
							<span class="cart-page-product order-total">Tổng tiền: </span>
							<strong class="cart-page-product order-price">120.000đ</strong>
						</div>

						<span class="cart-page-product order-note px-3 py-2">Giá đã bao gồm VAT (nếu có)</span>

						<div class="cart-submit px-3 py-4">
							<a class="btn btn-block btn-danger" href="#">Tiến hành thanh toán</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		<div style="margin-top: 320px">
	<jsp:include page="../layout/footer.jsp"></jsp:include>
	</div>

</body>

</html>