<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<base href="${pageContext.servletContext.contextPath}/">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Giỏ hàng</title>
</head>

<body>
    <jsp:include page="../layout/header.jsp"></jsp:include>

	<div class="container" style="padding-top: 120px;">
		<div class="shopping-cart tittle">
			<h5>Giỏ hàng ( 0 sản phẩm )</h5>
		</div>

		<div class="row">
			<div class="d-none d-lg-block d-xl-block col-3"></div>
			<div class="col-12 col-lg-6 col-xl-6">
				<div class="cart-empty" style="position: relative; text-align: center">
					<div style="text-align: center; padding: 35px 0px 0px;">
						<img src="images/shopping-cart.jpg" style="width: 80%">
					</div>
					<div style="line-height: 1.2em; padding: 5px; font-size: 14px; text-align: center;">
						Hãy mua sắm đi nào!!!<br>Có rất nhiều mẫu thiết kế dành riêng cho bạn.
					</div>

					<a href="index.jsp" class="btn btn-danger btn-block btn-lg mt-3 btn-cart-continue-shopping">Tiếp tục mua sắm</a>

				</div>
			</div>
			<div class="d-none d-lg-block d-xl-block col-3"></div>
		</div>
	</div>
	
	<div style="margin-top: 400px">
	<jsp:include page="../layout/footer.jsp"></jsp:include>
	</div>

</body>

</html>