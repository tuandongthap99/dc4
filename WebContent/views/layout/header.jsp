<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="css/style.css">
	<base href="${pageContext.servletContext.contextPath}/">
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top m-0 p-0" id="navbar_main">
			<div class="container">
				<a class="navbar-brand" href="index.jsp"> <img alt=""
						src="https://cdn.iconscout.com/icon/free/png-256/olympic-sign-olympics-logo-play-sport-event-5-25556.png"
						width="80px" height="80px">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#">Trang chủ</a>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Áo câu lạc bộ
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Giải Ngoại Hạng Anh (Pr.League)</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Giải Bóng Đá V-League</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Giải Tây Ban Nha (Laliga)</a>
							</div>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Áo đội tuyển quốc gia
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Áo đội tuyển Châu Á</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Áo đội tuyển Châu Âu</a>
							</div>
						</li>
					</ul>

					<ul class="navbar-nav mt-2">
						<li class="nav-item dropdown">
							<a class="nav-link" style="border-right: 2px solid black; height: 30px; padding-top: 2px"
								href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								<i class="fa fa-user-circle" style="font-size: 25px;"></i>
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" data-toggle="modal" data-target="#exampleModal" href="#"> Đăng nhập</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" data-toggle="modal" data-target="#exampleModal1" href="#">Đăng ký</a>
							</div>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="views/carts/cart.jsp" style="padding-top: 2px"><i class="fa fa-cart-plus"
									style="font-size: 25px;"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<!-- Login -->
	<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document" style="max-width: 70%">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6 p-0 m-0">
						     <img src="images/image_01.jpg" class="rounded col-sm-12" style="height: 650px; width: 550px">
						</div>
						<div class="col-sm-6 p-0 m-0">
							<h3 class="text-center">ĐĂNG NHẬP HỆ THỐNG</h3>
							<form action="" method="post">
								<div class="form-group col-sm-12 mt-4">
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-phone"></i>
											</span>
										</div>
										<input type="text" class="form-control" name="phone" id="phone"  placeholder="Nhập số điện thoại">
									</div>
								</div>
								<div class="form-group col-sm-12 mt-4">
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-lock"></i>
											</span>
										</div>
										<input type="password" class="form-control" name="password" id="password"  placeholder="Nhập mật khẩu">
									</div>
								</div>

								<div class="form-group col-sm-12">
								    <button type="button" class="btn btn-primary btn-block">ĐĂNG NHẬP</button>
								</div>
								
								<div class="form-group col-sm-12 text-center">
									<div class="hr-sect">Hoặc đăng nhập nhanh bằng</div>
								</div>
								
								<div class="form-group col-sm-12 row">
								<div class="col-sm-6">
								     <button class="btn btn-block btn-primary btn-facebook" type="button">
                                         <i class="fa fa-facebook"></i>
                                         <span>Facebook</span>
                                     </button>
                                 </div>
                                     <div class="col-sm-6">
                                         <button class="btn btn-block btn-danger btn-google-plus" type="button">
                                              <i class="fa fa-google-plus"></i>
                                              <span>Google+</span>
                                         </button>
                                     </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<!-- 	Register -->
	<div class="modal fade" id="exampleModal1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document" style="max-width: 70%">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6 p-0 m-0">
						     <img src="images/image_01.jpg" class="rounded col-sm-12" style="height: 650px; width: 550px">
						</div>
						<div class="col-sm-6 p-0 m-0">
							<h3 class="text-center">Đăng ký tài khoản</h3>
							<form action="" method="post">
								<div class="form-group col-sm-12 mt-4">
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-phone"></i>
											</span>
										</div>
										<input type="text" class="form-control" name="phone" id="phone"  placeholder="Nhập số điện thoại">
									</div>
								</div>
								<div class="form-group col-sm-12 mt-4">
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-lock"></i>
											</span>
										</div>
										<input type="password" class="form-control" name="password" id="password"  placeholder="Nhập mật khẩu">
									</div>
								</div>
								<div class="form-group col-sm-12 mt-4">
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-lock"></i>
											</span>
										</div>
										<input type="password" class="form-control" name="password" id="password"  placeholder="Nhập lại mật khẩu">
									</div>
								</div>
								<div class="form-group col-sm-12 mt-4">
									<div class="input-group">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-user"></i>
											</span>
										</div>
										<input type="password" class="form-control" name="password" id="password"  placeholder="Nhập tên của bạn">
									</div>
								</div>

								<div class="form-group col-sm-12">
								    <button type="button" class="btn btn-primary btn-block">Tạo tài khoản</button>
								</div>
								
								<div class="form-group col-sm-12 text-center">
									<div class="hr-sect"><a href="#" class="text-primary">Đăng nhập</a> Hoặc <a href="#" class="text-primary">Quên mật khẩu</a></div>
								</div>
								
								<div class="form-group col-sm-12 row">
								<div class="col-sm-6">
								     <button class="btn btn-block btn-primary btn-facebook" type="button">
                                         <i class="fa fa-facebook"></i>
                                         <span>Facebook</span>
                                     </button>
                                 </div>
                                     <div class="col-sm-6">
                                         <button class="btn btn-block btn-danger btn-google-plus" type="button">
                                              <i class="fa fa-google-plus"></i>
                                              <span>Google+</span>
                                         </button>
                                     </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>