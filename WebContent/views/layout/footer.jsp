<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<base href="${pageContext.servletContext.contextPath}/">
</head>
<body>
		<section id="footer">
				<div class="container">
					<div class="row text-center text-xs-center text-sm-left text-md-left">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<h5>Về chúng tôi</h5>
							<ul class="list-unstyled quick-links">
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Giới thiệu</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Liên hệ</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Đối tác</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Bảo mật</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Điều khoản</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4">
							<h5>Hỗ trợ khách hàng</h5>
							<ul class="list-unstyled quick-links">
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Chính sách bán hàng</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Câu hỏi thường gặp</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Tư vấn sản phẩm</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4">
							<h5>Kết nối cùng chúng tôi</h5>
							<ul class="list-unstyled quick-links">
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Home</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>About</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
								<li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>Get Started</a></li>
								<li><a href="https://wwwe.sunlimetech.com" title="Design and developed by"><i class="fa fa-angle-double-right"></i>Imprint</a></li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
							<ul class="list-unstyled list-inline social text-center">
								<li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-instagram"></i></a></li>
								<li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-google-plus"></i></a></li>
								<li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
							</ul>
						</div>
						</hr>
					</div>	
				</div>
			</section>

</body>
</html>